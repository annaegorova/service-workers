import { expect } from 'chai';
import '@babel/polyfill';
import sinon from 'sinon';
import { validate, validateAsync, validateWithThrow, validateWithLog } from '../email-validator';

const positiveTestData = [
  'name@gmail.com',
  'name@outlook.com',
  'name@yandex.ru'
];
const negativeTestData = [
  'name@mail.ru',
  'some string',
  ''
];

describe('validate', () => {
  positiveTestData.forEach(email => {
    it(`should return true on valid email '${email}'`, () => {
      expect(validate(email)).to.equal(true);
    });
  });
  negativeTestData.forEach(email => {
    it(`should return false on invalid email '${email}'`, () => {
      expect(validate(email)).to.equal(false);
    });
  });
});

describe('validateAsync', () => {
  positiveTestData.forEach(email => {
    it(`should return true on valid email '${email}'`, async () => {
      const result = await validateAsync(email);
      expect(result).to.equal(true);
    });
  });
  negativeTestData.forEach(email => {
    it(`should return false on invalid email '${email}'`, async () => {
      const result = await validateAsync(email);
      expect(result).to.equal(false);
    });
  });
});

describe('validateWithThrow', () => {
  positiveTestData.forEach(email => {
    it(`should return true on valid email '${email}'`, () => {
      expect(validateWithThrow(email)).to.equal(true);
    });
  });
  negativeTestData.forEach(email => {
    it(`should throw error on invalid email '${email}'`, () => {
      expect(() => validateWithThrow(email)).to.throw();
    });
  });
});

describe('validateWithLog', () => {
  const spy = sinon.spy(console, 'log');
  afterEach( () => spy.restore());
  positiveTestData.forEach(email => {
    it(`should log true on valid email '${email}'`, () => {
      validateWithLog(email);
      expect(spy.calledWith(true));
    });
  });
  negativeTestData.forEach(email => {
    it(`should log false on invalid email '${email}'`, () => {
      validateWithLog(email);
      expect(spy.calledWith(false));
    });
  });
});
