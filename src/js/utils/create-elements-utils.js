export function createElement(elemType, classNames, innerText) {
  const elem = document.createElement(elemType);

  if (classNames) {
      Array.isArray(classNames) ?
        elem.classList.add(...classNames) :
        elem.classList.add(classNames);
  }

  if (innerText) {
    elem.innerText = innerText;
  }

  return elem;
}

export function addNewElement(parentElem, elemType, classNames, innerText) {
  parentElem.appendChild(createElement(elemType, classNames, innerText));
}
