import { addNewElement, createElement } from './create-elements-utils';
import {
  APP_CONTAINER,
  APP_SECTION,
  APP_SUBTITLE,
  APP_TITLE,
  COMMUNITY_SECTION,
  COMMUNITY_SECTION_CONTAINER,
  COMMUNITY_SECTION_MEMBERS,
  COMMUNITY_SECTION_MEMBERS_AVATAR,
  COMMUNITY_SECTION_MEMBERS_DESCRIPTION,
  COMMUNITY_SECTION_MEMBERS_NAME,
  COMMUNITY_SECTION_MEMBERS_POSITION,
  CULTURE_SECTION
} from '../constants/selectors-constants';
import {
  COMMUNITY_SECTION_DESCRIPTION,
  COMMUNITY_SECTION_HEADER,
  COMMUNITY_SECTION_MEMBER_INFO
} from '../constants/content-constants';
import { getData } from './fetch-data-utils';

export async function addCommunitySection() {
  try {
    const responseData = await getData('http://localhost:8080/api/community');

    const section = createElement('section', [APP_SECTION, COMMUNITY_SECTION]);
    addNewElement(section, 'h2', APP_TITLE, COMMUNITY_SECTION_HEADER);
    addNewElement(section, 'h3', APP_SUBTITLE, COMMUNITY_SECTION_DESCRIPTION);
    section.appendChild(createMembersPanes(responseData));

    appendCommunitySection(section);
  } catch (error) {
    console.log('Error when getting communities:', error); // for now let's console
  }
}

function createMembersPanes(membersData) {
  const container = createElement('div', [COMMUNITY_SECTION_CONTAINER]);

  membersData.forEach(member => {
    const memberPane = createElement('div', COMMUNITY_SECTION_MEMBERS);
    memberPane.appendChild(createAvatar(member.avatar));
    addNewElement(
        memberPane,
        'div',
        COMMUNITY_SECTION_MEMBERS_DESCRIPTION,
        COMMUNITY_SECTION_MEMBER_INFO
    );
    addNewElement(
        memberPane,
        'div',
        COMMUNITY_SECTION_MEMBERS_NAME,
        `${member.firstName} ${member.lastName}`
    );
    addNewElement(
        memberPane,
        'div',
        COMMUNITY_SECTION_MEMBERS_POSITION,
        member.position
    );
    container.appendChild(memberPane);
  });

  return container;
}

function createAvatar(avatarSrc) {
  const avatar = createElement('img', COMMUNITY_SECTION_MEMBERS_AVATAR);
  avatar.setAttribute('src', avatarSrc);
  return avatar;
}

function appendCommunitySection(section) {
  const app = document.getElementById(APP_CONTAINER);
  const cultureSection = document.getElementsByClassName(CULTURE_SECTION)[0];
  app.insertBefore(section, cultureSection);
}
