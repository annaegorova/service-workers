import { postData } from './utils/fetch-data-utils';

self.addEventListener('install', event => {
  console.log('Install event.');
});

self.addEventListener('activate', event => {
  console.log('Activate event');
});

self.addEventListener('sync', event => {
  const { action, payload } = JSON.parse(event.tag);
  switch (action) {
    case 'subscribe':
      event.waitUntil(postData('api/subscribe', { email: payload })
          .catch(() => sendMessageToMain({ action: 'subscribeFailed', payload })));
      break;
    case 'unsubscribe':
      event.waitUntil(postData('api/unsubscribe', { email: payload })
          .catch(() => sendMessageToMain({ action: 'unsubscribeFailed', payload })));
      break;
    default:
      console.log('Invalid action type in sync.');
  }
});

function sendMessageToMain(message) {
  clients.matchAll({
    includeUncontrolled: false,
    type: 'window'
  }).then(clients => {
    clients.forEach(client => client.postMessage(message));
  });
}
