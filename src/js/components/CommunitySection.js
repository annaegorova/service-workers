import { COMMUNITY_SECTION } from '../constants/selectors-constants';
import { addCommunitySection } from '../utils/community-section';
import { Section } from './Section';

export class CommunitySection extends Section {
  constructor() {
    super(addCommunitySection, COMMUNITY_SECTION);
  }
}
