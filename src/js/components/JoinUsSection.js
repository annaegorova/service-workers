import { JOIN_PROGRAM_SECTION } from '../constants/selectors-constants';
import { addJoinUsSection } from '../utils/join-us-section';
import { Section } from './Section';

export class JoinUsSection extends Section {
  constructor(headerText, buttonText) {
    super(() => addJoinUsSection(headerText, buttonText), JOIN_PROGRAM_SECTION);
  }
}
